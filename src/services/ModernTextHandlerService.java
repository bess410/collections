package services;

import interfaces.TextHandlerService;

import java.util.*;
import java.util.stream.Collectors;

public class ModernTextHandlerService implements TextHandlerService {
    @Override
    public List<String> getSortedList(List<String> list) {
        return list.stream()
                .sorted()
                .collect(Collectors.toList());
    }

    @Override
    public List<String> getReversedSortedList(List<String> list) {
        return list.stream()
                .sorted(Comparator.reverseOrder())
                .collect(Collectors.toList());
    }

    @Override
    public Map<String, Long> getNumberWordRepetitions(List<String> text) {
        Map<String, Long> map;
        map = text.stream()
                .map(String::toLowerCase)
                .flatMap(line -> Arrays.stream(line.split("\\W+")))
                .filter(word -> word.length() != 0)
                .collect(Collectors.toMap(
                        word -> word,
                        word -> 1L,
                        (value1, value2) -> value1 + value2));
        return map;
    }

    @Override
    public List<String> getUniqueWords(List<String> text) {
        List<String> uniqueWords = new ArrayList<>();
        uniqueWords.addAll(getNumberWordRepetitions(text).keySet());
        return uniqueWords;
    }
}
