package services;

import interfaces.FileHandlerService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class ModernFileHandlerService implements FileHandlerService {
    @Override
    public List<String> getTextFromFile(String filename) {
        List<String> text = new ArrayList<>();
        try {
            text = Files.lines(Paths.get(filename)).collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return text;
    }
}
