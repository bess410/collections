package services;

import interfaces.TextHandlerService;

import java.util.*;

public class SimpleTextHandlerService implements TextHandlerService {
    @Override
    public List<String> getSortedList(List<String> list) {
        Collections.sort(list);
        return list;
    }

    @Override
    public List<String> getReversedSortedList(List<String> list) {
        list.sort(Comparator.reverseOrder());
        return list;
    }

    @Override
    public Map<String, Long> getNumberWordRepetitions(List<String> text) {
        Map<String, Long> map = new HashMap<>();
        String[] arrayString;
        for (String line : text) {
            arrayString = line.toLowerCase().split("\\W+");
            for (String str : arrayString) {
                if (str.length() == 0) {
                    continue;
                }
                if (map.containsKey(str)) {
                    map.put(str, map.get(str) + 1);
                } else {
                    map.put(str, 1L);
                }
            }
        }
        return map;
    }

    @Override
    public List<String> getUniqueWords(List<String> text) {
        List<String> uniqueWords = new ArrayList<>();
        uniqueWords.addAll(getNumberWordRepetitions(text).keySet());
        return uniqueWords;
    }
}
