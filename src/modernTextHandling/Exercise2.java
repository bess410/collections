package modernTextHandling;

import interfaces.FileHandlerService;
import interfaces.TextHandlerService;
import services.ModernFileHandlerService;
import services.ModernTextHandlerService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/*Необходимо выбрать в текст (длинной не менее 1000 символов). В данном тексте необходимо:
     2. Вывести все уникальные слова из текста.
        Рекомендуется использовать List<String>.*/

public class Exercise2 {
    public static void main(String[] args) {
        String filename = "files/One thousand dollars.txt";
        // Создаем сервис для работы с нашим файлом
        FileHandlerService fileService = new ModernFileHandlerService();
        // Получаем текст из файла
        List<String> text = fileService.getTextFromFile(filename);
        // Создаем сервис для работы с текстом
        TextHandlerService textService = new ModernTextHandlerService();
        // Получаем уникальные слова из текста
        List<String> uniqueWords = textService.getUniqueWords(text);
        System.out.println(uniqueWords);
    }
}
