package textHandling;

import interfaces.FileHandlerService;
import interfaces.TextHandlerService;
import services.SimpleFileHandlerService;
import services.SimpleTextHandlerService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/* Необходимо выбрать в текст (длинной не менее 1000 символов). В данном тексте необходимо:
    3*. Отсортировать все слова из текста по убыванию
        (Подсказка: лучше и проще использовать Comparator) */

public class Exercise3 {
    public static void main(String[] args) {
        String filename = "files/One thousand dollars.txt";
        // Создаем сервис для работы с нашим файлом
        FileHandlerService fileService = new SimpleFileHandlerService();
        // Получаем текст из файла
        List<String> text = fileService.getTextFromFile(filename);
        // Создаем сервис для работы с текстом
        TextHandlerService textService = new SimpleTextHandlerService();
        // Получаем список слов
        List<String> words = textService.getUniqueWords(text);
        // Сортируем слова по убыванию
        List<String> sortedList = textService.getReversedSortedList(words);
        System.out.println(sortedList);
    }
}