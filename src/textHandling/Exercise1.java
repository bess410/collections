package textHandling;

import interfaces.TextHandlerService;
import services.SimpleFileHandlerService;
import interfaces.FileHandlerService;
import services.SimpleTextHandlerService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*Необходимо выбрать в текст (длинной не менее 1000 символов). В данном тексте необходимо:
    1.  Подсчитать количество повторений для каждого слова -
        (принять, что знаки пробела, знаки препинания являются разделителями слов).
        Рекомендуется использовать Map<String, Long>.*/

public class Exercise1 {
    public static void main(String[] args) {
        String filename = "files/One thousand dollars.txt";
        // Создаем сервис для работы с нашим файлом
        FileHandlerService fileService = new SimpleFileHandlerService();
        // Получаем текст из файла
        List<String> text = fileService.getTextFromFile(filename);
        // Создаем сервис для работы с текстом
        TextHandlerService textService = new SimpleTextHandlerService();
        // Получаем количество повторений слов в тексте
        Map<String, Long> wordRepetitions = textService.getNumberWordRepetitions(text);
        System.out.println(wordRepetitions);
    }
}
