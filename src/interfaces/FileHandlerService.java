package interfaces;

import java.io.IOException;
import java.util.List;

public interface FileHandlerService {
    List<String> getTextFromFile(String filename);
}
