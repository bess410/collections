package interfaces;

import java.util.List;
import java.util.Map;

public interface TextHandlerService {
    Map<String, Long> getNumberWordRepetitions(List<String> text);

    List<String> getUniqueWords(List<String> text);

    List<String> getSortedList(List<String> list);

    List<String> getReversedSortedList(List<String> list);
}
